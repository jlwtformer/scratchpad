﻿using ControlzEx.Theming;
using Fluent;
using Scratchpad.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Scratchpad
{
    public partial class MainWindow : RibbonWindow
    {
        bool isEdited = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_New_Click(object sender, RoutedEventArgs e)
        {
            if (isEdited)
            {
                // TODO Display message box asking if they want to save or discard
                mainTextArea.Document.Blocks.Clear();
                isEdited = false;
                Title = "Untitled";
            }
            else mainTextArea.Document.Blocks.Clear();
        }

        private void Button_New_Window_Click(object sender, RoutedEventArgs e)
        {
            // TODO Start new blank instance
        }

        private void Button_Open_Click(object sender, RoutedEventArgs e)
        {
            TextFileMethods.OpenTextFile(mainTextArea, this);
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            TextFileMethods.SaveTextFile(mainTextArea, this);
        }
    }
}
