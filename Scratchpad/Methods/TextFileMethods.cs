﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;

namespace Scratchpad.Methods
{
    public class TextFileMethods
    {
        public static void OpenTextFile(System.Windows.Controls.RichTextBox textBox, Window window)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Title = "Open Text File",
                Filter = "Text File|*.txt"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string shortName = openFileDialog.FileName.Substring(openFileDialog.FileName.LastIndexOf('\\') + 1);

                if (File.Exists(openFileDialog.FileName))
                {
                    TextRange range = new TextRange(textBox.Document.ContentStart, textBox.Document.ContentEnd);
                    FileStream stream = new FileStream(openFileDialog.FileName, FileMode.OpenOrCreate);

                    range.Load(stream, System.Windows.Forms.DataFormats.Text);

                    stream.Close();
                }

                window.Title = shortName;
            }
        }

        public static void SaveTextFile(System.Windows.Controls.RichTextBox textBox, Window window)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Title = "Save Text File",
                DefaultExt = "txt"
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                TextRange range = new TextRange(textBox.Document.ContentStart, textBox.Document.ContentEnd);
                FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create);

                range.Save(stream, System.Windows.DataFormats.Text);
                stream.Close();

                string shortName = saveFileDialog.FileName.Substring(saveFileDialog.FileName.LastIndexOf('\\') + 1);
                window.Title = shortName;
            }
        }
    }
}
